const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .combine([
        'resources/admin/js/jquery.min.js',
        'resources/admin/js/jquery-ui.min.js',
        'resources/admin/js/bootstrap.min.js',
        'resources/admin/js/bootstrap-datepicker.min.js',
        'resources/admin/js/daterangepicker.min.js',
        'resources/admin/js/adminlte.min.js',
        'resources/admin/js/dashboard.js',
        'resources/admin/js/demo.js',
    ], 'public/js/admin.js')
    .combine([
        'resources/admin/css/bootstrap.min.css',
        'resources/admin/css/_all-skins.min.css',
        'resources/admin/css/AdminLTE.min.css',
        'resources/admin/css/bootstrap-datepicker.min.css',
        'resources/admin/css/daterangepicker.min.css',
        'resources/admin/css/font-awesome.min.css',
        'resources/admin/css/ionicons.min.css',
    ], 'public/css/admin.css')
    .combine([
        'resources/ny/css/bootstrap.min.css',
        'resources/ny/css/font-awesome.min.css',
        'resources/ny/css/style.css',
        'resources/ny/css/animate.css',
        'resources/ny/css/hamburgers.min.css',
        'resources/ny/css/animsition.min.css',
        'resources/ny/css/select2.min.css',
        'resources/ny/css/daterangepicker.css',
        'resources/ny/css/slick.css',
        'resources/ny/css/lightbox.min.css',
        'resources/ny/css/util.css',
        'resources/ny/css/main.css'
    ], 'public/css/ny.css')
    .combine([
        'resources/ny/js/jquery-3.2.1.min.js',
        'resources/ny/js/animsition.min.js',
        'resources/ny/js/popper.js',
        'resources/ny/js/bootstrap.min.js',
        'resources/ny/js/select2.min.js',
        'resources/ny/js/slick.min.js',
        'resources/ny/js/slick-custom.min.js',
        'resources/ny/js/countdowntime.js',
        'resources/ny/js/lightbox.min.js',
        'resources/ny/js/sweetalert.min.js',
        'resources/ny/js/main.js'
    ], 'public/js/ny.js')
    // .combine([
    //     'resources/admin-01/css/bootstrap.min.css',
    //     'resources/admin-01/css/bootstrap-wysihtml5.css',
    //     'resources/admin-01/css/colorpicker.css',
    //     'resources/admin-01/css/datapicker.css',
    //     'resources/admin-01/css/font-awesome.css',
    //     'resources/admin-01/css/fullcalendar.css',
    //     'resources/admin-01/css/jquery.easy-pie-chart.css',
    //     'resources/admin-01/css/jquery.gritter.css',
    //     'resources/admin-01/css/matrix-login.css',
    //     'resources/admin-01/css/matrix-media.css',
    //     'resources/admin-01/css/matrix-style.css',
    //     'resources/admin-01/css/select2.css',
    //     'resources/admin-01/css/uniform.css'
    // ], 'public/css/admin-01.css')
    // .combine([
    //     'resources/admin-01/js/excanvas.min.js',
    //     'resources/admin-01/js/jquery.ui.custom.js',
    //     'resources/admin-01/js/bootstrap.min.js',
    //     'resources/admin-01/js/jquery.flot.min.js',
    //     'resources/admin-01/js/jquery.flot.resize.min.js',
    //     'resources/admin-01/js/jquery.peity.min.js',
    //     'resources/admin-01/js/fullcalendar.min.js',
    //     'resources/admin-01/js/matrix.js',
    //     'resources/admin-01/js/matrix.dashboard.js',
    //     'resources/admin-01/js/jquery.gritter.min.js',
    //     'resources/admin-01/js/matrix.interface.js',
    //     'resources/admin-01/js/matrix.chat.js',
    //     'resources/admin-01/js/jquery.validate.js',
    //     'resources/admin-01/js/matrix.form_validation.js',
    //     'resources/admin-01/js/jquery.wizard.js',
    //     'resources/admin-01/js/jquery.uniform.js',
    //     'resources/admin-01/js/select2.min.js',
    //     'resources/admin-01/js/matrix.popover.js',
    //     'resources/admin-01/js/jquery.dataTables.min.js',
    //     'resources/admin-01/js/matrix.tables.js',
    //     'resources/admin-01/js/popper.min.js'
    // ], 'public/js/admin-01.js')
    .combine([
        'resources/admin-01/js/jquery.min.js',
        'resources/admin-01/js/jquery.ui.custom.js',
        'resources/admin-01/js/bootstrap.min.js',
        'resources/admin-01/js/jquery.uniform.js',
        'resources/admin-01/js/select2.min.js',
        'resources/admin-01/js/jquery.dataTables.min.js',
        'resources/admin-01/js/matrix.tables.js',
        'resources/admin-01/js/matrix.js',
    ], 'public/js/js-table.js')
   .sass('resources/sass/app.scss', 'public/css');

