<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            factory(App\User::class)->create([
                    "name" => "admin",
                    "email" => "admin@gmail.com",
                    "password" => bcrypt(env('ADMIN_PWD', '123456'))]
            );
    }
}
