<?php

use Illuminate\Database\Seeder;

class DataDemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Category::create([
                "name" => "danh mục 1"]
        );
        \App\Category::create([
                "name" => "danh mục 2"]
        );
        \App\Product::create([
                "name" => "sản phẩm 1",
                "unit_price" => "100000",
                "promotion_price" => "75000",
                "unit" => "100"]
        );
        \App\Product::create([
                "name" => "sản phẩm 2",
                "unit_price" => "200000",
                "unit" => "100"]
        );
        \App\Product::create([
                "name" => "sản phẩm 3",
                "unit_price" => "500000",
                "promotion_price" => "450000",
                "unit" => "100"]
        );
    }
}
