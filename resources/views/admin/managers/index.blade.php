@extends('admin.layout.master')
@section('content')
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Danh sách</h3>
                    <div class="box-tools">
                        {!! Form::open(['method' => 'GET', 'url' => 'admin/managers', 'class' => 'pull-left', 'role' => 'search'])  !!}
                        <div class="input-group" style="width: 200px;">
                            <input type="text" value="{{\Request::get('search')}}" class="form-control input-sm" name="search" placeholder="Tìm kiếm">
                            <span class="input-group-btn">
                        <button class="btn btn-default btn-sm" type="submit">
                            <i class="fa fa-search"></i> Tìm
                        </button>
                    </span>
                        </div>
                        {!! Form::close() !!}
                            <a href="{{ url('admin/managers/create') }}" class="btn btn-success btn-sm" title="Thêm mới">
                                <i class="fa fa-plus" aria-hidden="true"></i> <span class="hidden-xs">Thêm mới</span>
                            </a>
                    </div>
                </div>
                @php($index = ($managers->currentPage()-1)*$managers->perPage())
                <div class="box-body table-responsive no-padding">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th class="text-center">STT</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>Updated_at</th>
                            <th></th>
                        </tr>
                        @foreach($managers as $item)
                            <tr>
                                <td class="text-center">{{ ++$index }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->email }}</td>
                                <td>{{ $item->password }}</td>
                                <td>{{ Carbon\Carbon::parse($item->updated_at)->format("d/m/Y H:i") }}</td>
                                <td>
                                    <a href="{{ url('admin/managers/' . $item->id) }}" title="Show"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> show</button></a>
                                    <a href="{{ url('admin/managers/' . $item->id . '/edit') }}" title="Edit"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> edit</button></a>
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['admin/managers', $item->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> '."delete", array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-xs',
                                            'title' => __('message.delete'),
                                            'onclick'=>'return confirm("Bạn có muốn xóa phần tử này không?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="box-footer clearfix">
                        {!! $managers->appends(\Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
@endsection

