@extends('admin.layout.master')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Chi tiết</h3>
            <div class="box-tools">
                <a href="{{ url('admin/managers') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
                    <a href="{{ url('/admin/managers/' . $manager->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">sữa</span></a>
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['admin/managers/', $manager->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">xóa</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("Bạn có muốn xóa phẩn tử này không")'
                    ))!!}
                    {!! Form::close() !!}
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th> Username </th>
                    <td> {{ $manager->name }} </td>
                </tr>
                <tr>
                    <th> Email </th>
                    <td> {{ $manager->email }} </td>
                </tr>
                <tr>
                    <th> Password </th>
                    <td> {{ $manager->password }} </td>
                </tr>
                <tr>
                    <th> Ngày cập nhật </th>
                    <td> {{ Carbon\Carbon::parse($manager->updated_at)->format("d/m/Y H:i") }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection
