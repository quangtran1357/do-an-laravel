@extends('admin.layout.master')
@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Cập nhật</h3>
            <div class="box-tools">
                <a href="{{ url('/admin/managers') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
            </div>
        </div>

        {!! Form::model($manager, [
            'method' => 'PATCH',
            'url' => ['/admin/managers', $manager->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}

        @include ('admin.managers.form', ['submitButtonText' => 'Cập nhật'])

        {!! Form::close() !!}
    </div>
@endsection
