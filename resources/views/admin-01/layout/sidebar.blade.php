<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
    <ul>
        <li><a href="{{ url('admin') }}"><i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span></a> </li>
        <li> <a href="#"><i class="fa fa-inbox" aria-hidden="true"></i> <span>Products</span></a> </li>
        <li> <a href="#"><i class="fa fa-th-list" aria-hidden="true"></i> <span>Categories</span></a> </li>
        <li><a href="#"><i class="fa fa-file-text-o"></i> <span>Bills</span></a></li>
        <li><a href="#"><i class="fa fa-users"></i> <span>Customers</span></a></li>
        <li><a href="#"><i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>News</span></a></li>
        <li><a href="#"><i class="fa fa-sliders" aria-hidden="true"></i> <span>Slides</span></a></li>
        <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Managers</span> <span class="label label-important">5</span></a>
            <ul>
                <li><a href="{{ url('admin/managers') }}">Admin</a></li>
                <li><a href="#">Infor</a></li>
                <li><a href="#">User</a></li>
            </ul>
        </li>
        <li class="submenu"> <a href="#"><i class="icon icon-info-sign"></i> <span>Error</span> <span class="label label-important">4</span></a>
            <ul>
                <li><a href="error403.html">Error 403</a></li>
                <li><a href="error404.html">Error 404</a></li>
                <li><a href="error405.html">Error 405</a></li>
                <li><a href="error500.html">Error 500</a></li>
            </ul>
        </li>
        <li class="content"> <span>Monthly Bandwidth Transfer</span>
            <div class="progress progress-mini progress-danger active progress-striped">
                <div style="width: 77%;" class="bar"></div>
            </div>
            <span class="percent">77%</span>
            <div class="stat">21419.94 / 14000 MB</div>
        </li>
        <li class="content"> <span>Disk Space Usage</span>
            <div class="progress progress-mini active progress-striped">
                <div style="width: 87%;" class="bar"></div>
            </div>
            <span class="percent">87%</span>
            <div class="stat">604.44 / 4000 MB</div>
        </li>
    </ul>
</div>
<!--sidebar-menu-->
