<!DOCTYPE html>
<html lang="en">
<head>
    <title>Matrix Admin</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    {{--<link href="font-awesome/css/font-awesome.css" rel="stylesheet" />--}}
    <script src="https://use.fontawesome.com/9af01c9f93.js"></script>
    <link href="{{ asset('css/admin-01.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style-login.css') }}" rel="stylesheet">
    <link href="{{ asset('css/add.css') }}" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
</head>
<body>

@include('admin-01.layout.header')

@include('admin-01.layout.sidebar')

@yield('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="{{ asset('js/admin-01.js') }}"></script>
@yield('script')
<script>
    // console.log(window.location.href.split("/"));
    // $('#sidebar ul li a[href="/' + window.location.href + '"]').addClass('active');
    var path = window.location.href; // because the 'href' property of the DOM element is the absolute path
    $('#sidebar ul li a').each(function() {
        if (this.href === path) {
            $(this).parent().addClass('active');
        }
    });
</script>
<script type="text/javascript">
    // This function is called from the pop-up menus to transfer to
    // a different page. Ignore if the value returned is a null string:
    function goPage (newURL) {

        // if url is empty, skip the menu dividers and reset the menu selection to default
        if (newURL != "") {

            // if url is "-", it is this page -- reset the menu:
            if (newURL == "-" ) {
                resetMenu();
            }
            // else, send page to designated URL
            else {
                document.location.href = newURL;
            }
        }
    }

    // resets the menu selection upon entry to this page:
    function resetMenu() {
        document.gomenu.selector.selectedIndex = 2;
    }
</script>
</body>
</html>
