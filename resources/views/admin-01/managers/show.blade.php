@extends('adminlte::layouts.app')
@section('htmlheader_title')
    {{ __('flight::flights.flight') }}
@endsection
@section('contentheader_title')
    {{ __('flight::flights.flight') }}
@endsection
@section('contentheader_description')

@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li><a href="{{ url("home") }}"><i class="fa fa-home"></i> {{ __("message.dashboard") }}</a></li>
        <li><a href="{{ url('/flights') }}">{{ __('flight::flights.flight') }}</a></li>
        <li class="active">{{ __("message.detail") }}</li>
    </ol>
@endsection
@section('main-content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">{{ __("message.detail") }}</h3>
            <div class="box-tools">
                <a href="{{ url('/flights') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">{{ trans('message.lists') }}</span></a>
                @can('FlightController@update')
                    <a href="{{ url('/flights/' . $flight->id . '/edit') }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="hidden-xs">{{ __('message.edit') }}</span></a>
                @endcan
                @can('FlightController@destroy')
                    {!! Form::open([
                        'method'=>'DELETE',
                        'url' => ['flights', $flight->id],
                        'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> <span class="hidden-xs">'.__('message.delete').'</span>', array(
                            'type' => 'submit',
                            'class' => 'btn btn-danger btn-sm',
                            'title' => __('message.delete'),
                            'onclick'=>'return confirm("'.__('message.confirm_delete').'")'
                    ))!!}
                    {!! Form::close() !!}
                @endcan
            </div>
        </div>
        <div class="box-body table-responsive no-padding">
            <table class="table table-striped">
                <tbody>
                <tr>
                    <th> {{ trans('flight::flights.name') }} </th>
                    <td> {{ $flight->name }} </td>
                </tr>
                <tr>
                    <th> {{ trans('flight::flights.city_start_id') }} </th>
                    <td> {{ $flight->city_start_id ? $flight->cityStart->name : '' }} </td>
                </tr>
                <tr>
                    <th> {{ trans('flight::flights.city_end_id') }} </th>
                    <td> {{ $flight->city_end_id ? $flight->cityEnd->name : '' }} </td>
                </tr>
                <tr>
                    <th> {{ trans('flight::flights.airlines') }} </th>
                    <td> {{ optional($flight->airlines)->implode('name', ', ') }} </td>
                </tr>
                <tr>
                    <th> {{ trans('flight::flights.managers') }} </th>
                    <td> {{ optional($flight->managers)->implode('name', ', ') }} </td>
                </tr>
                <tr>
                    <th> {{ trans('flight::flights.updated_at') }} </th>
                    <td> {{ Carbon\Carbon::parse($flight->updated_at)->format(config('settings.format.datetime')) }} </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection