@extends('admin-01.layout.master')
@section('content')
    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#" class="current">Tables</a> </div>
        </div>
        {{--<div class="container-fluid">--}}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Thêm mới</h3>
                    <div class="box-tools">
                        <a href="{{ url('admin/managers') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> <span class="hidden-xs">Danh sách</span></a>
                    </div>
                </div>

                {!! Form::open(['url' => '/admin/managers', 'class' => 'form-horizontal', 'files' => true]) !!}

                @include('admin-01.managers.form')

                {!! Form::close() !!}
            </div>
        {{--</div>--}}
    </div>
@endsection
